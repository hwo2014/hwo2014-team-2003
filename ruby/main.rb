require "json"
require "socket"

server_host = ARGV[0]
server_port = ARGV[1]
bot_name = ARGV[2]
bot_key = ARGV[3]

puts "Connecting to #{server_host}:#{server_port} as player #{bot_name.inspect}"

class JoshBot
  def initialize(server_host, server_port, bot_name, bot_key)
    @bot_name = bot_name
    @bot_key = bot_key
    @tcp = TCPSocket.open(server_host, server_port)
    play
  end

  private

  def play
    @tcp.puts(join_message(@bot_name, @bot_key))
    react_to_messages_from_server
  end

  def react_to_messages_from_server
    while json = @tcp.gets
      message = JSON.parse(json)
      msg_type = message["msgType"]
      msg_data = message["data"]
      case msg_type
      when "carPositions"
        msg_data.each do |car_pos|
          if car_pos["id"]["name"] == @bot_name
            @angle = car_pos["angle"]
            @piece_index = car_pos["piecePosition"]["pieceIndex"]
          end
        end
        if @pieces[@piece_index]["radius"]
          @tcp.puts(throttle_message(0.2))
        else
          @tcp.puts(throttle_message(1.0))
        end
      else
        case msg_type
        when "gameInit"
          puts "Race is #{msg_data["race"]["raceSession"]["laps"]} laps on map #{msg_data["race"]["track"]["name"].inspect}"
          @pieces = msg_data["race"]["track"]["pieces"]
        when "join"
          puts "Joined"
        when "gameStart"
          puts "Race started"
        when "crash"
          puts "Someone crashed"
        when "gameEnd"
          puts "Race ended"
        when "error"
          puts "ERROR: #{msg_data}"
        end
        puts "Got #{msg_type}"
        @tcp.puts(ping_message)
      end
    end
  end

  def join_message(bot_name, bot_key)
    make_msg("join", name: bot_name, key: bot_key)
  end

  def throttle_message(throttle)
    make_msg("throttle", throttle)
  end

  def ping_message
    make_msg("ping", {})
  end

  def make_msg(msg_type, data)
    JSON.generate(msgType: msg_type, data: data)
  end
end

JoshBot.new(server_host, server_port, bot_name, bot_key)
